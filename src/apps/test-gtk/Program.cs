﻿using System;
using Gtk;

namespace test_gtk
{
    class Program
    {
        static void Main(string[] args)
        {
            Application.Init();
            MainWindow win = new MainWindow();
            win.Show();
            Application.Run();
        }
    }
}
