using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace vds.server.cli
{
    public class StorageDbContextFactory : IDesignTimeDbContextFactory<vds_storage.StorageDbContext>
    {
        public vds_storage.StorageDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<vds_storage.StorageDbContext>();
            optionsBuilder.UseSqlite("Data Source=local.db");

            return new vds_storage.StorageDbContext(optionsBuilder.Options);
        }
    }
}