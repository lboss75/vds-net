﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Data.Sqlite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.CommandLineUtils;

namespace vds.server.cli
{
    class Program
    {
        static void Main(string[] args)
        {
            var commandLineApplication = new CommandLineApplication(throwOnUnexpectedArg: true);
            commandLineApplication.Command("reset",
                (target) => {
                var login = target.Argument(
                    "login",
                    "Enter the login of the root user.",
                    multipleValues: false);
                var password = target.Argument(
                    "password",
                    "Enter the password of the root user.",
                    multipleValues: false);
                target.OnExecute(() => {
                    vds.server.Server.reset_server(login.Value, password.Value);
                    return 0;
                });
                });

                commandLineApplication.HelpOption("-? | -h | --help");

                commandLineApplication.Execute(args);
        }
    }
}
