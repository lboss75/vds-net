﻿using System;
using Gtk;
using vds.app;

namespace gtkclient
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            vds_app app = new vds_app();
            app.start();

            Application.Init();
            MainWindow win = new MainWindow();
            win.Show();
            Application.Run();
        }
    }
}
