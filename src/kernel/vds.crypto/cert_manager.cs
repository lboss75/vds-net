using System;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace vds.crypto
{
    public class cert_manager
    {
        public static X509Certificate2 create_root()
        {
            using (RSA rsa = RSA.Create())
            {
                var request = new CertificateRequest(
                    new X500DistinguishedName("CN=root"),
                    rsa,
                    HashAlgorithmName.SHA256,
                    RSASignaturePadding.Pkcs1);

                request.CertificateExtensions.Add(
                    new X509BasicConstraintsExtension(true, false, 0, true));

                return request.CreateSelfSigned(
                    DateTimeOffset.UtcNow,
                    new DateTimeOffset(2039, 12, 31, 23, 59, 59, TimeSpan.Zero));
            }
        }
    }
}