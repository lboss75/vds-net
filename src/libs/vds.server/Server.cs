﻿using System;

namespace vds.server
{
    public class Server
    {
        public static void reset_server(string login, string password){
            var builder = new ConfigurationBuilder();
                // .SetBasePath(System.IO.Directory.GetCurrentDirectory())
                // .AddJsonFile("appsettings.json")
                //.AddEnvironmentVariables()
                //.AddCommandLine(args, GetSwitchMappings(DefaultConfigurationStrings));

            var configuration = builder.Build();

            var services = ConfigureServices(new ServiceCollection())
                .BuildServiceProvider();

            using(var client = new vds_storage.StorageDbContext())
            {
                client.Database.EnsureCreated();
                //client.Database.Migrate();
            }

            Console.WriteLine("Press enter to exit");
            Console.ReadLine();            

        }
        static public IServiceCollection ConfigureServices(IServiceCollection registrator)
        {
            vds_storage.Storage.ConfigureServices(registrator);

            registrator
                // .AddEntityFramework()
                // .AddSqlite()
                .AddDbContext<vds_storage.StorageDbContext>(
                    options => options.UseSqlite(
                        new SqliteConnection(new SqliteConnectionStringBuilder()
                        {
                            DataSource = "local.db"
                        }
                        .ToString())
                ));

            return registrator;
        }
    }
}
