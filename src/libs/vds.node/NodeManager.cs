﻿using System;
using vds.transactions;

namespace vds.node
{
    public class NodeManager
    {
        public NodeManager()
        {

        }

        public void InitRoot()
        {
            var log = new TransactionLog();

            log.add(new AddRootUser());

            log.pack();
        }
    }
}
