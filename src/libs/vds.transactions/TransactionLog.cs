﻿using System;
using System.IO;
using System.IO.Compression;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace vds.transactions
{
    public class TransactionLog
    {
        private List<object> data_ = new List<object>();

        public TransactionLog()
        {

        }

        public void add(object message)
        {
            this.data_.Add(message);
        }

        public byte[] pack()
        {
            using(var key = CryptoService.create_key())
            {
                using(var stream = new CryptoStream(MyStream, key.CreateEncryptor(), CryptoStreamMode.Write))
                {

                }
            }

            using(var target = new MemoryStream())
            {
                using(var compressionStream = new DeflateStream(target, CompressionMode.Compress))
                {
                    var body = JsonConvert.SerializeObject(this.data_);
                    var body_data = Encoding.UTF8.GetBytes(body);
                    compressionStream.Write(body_data, 0, body_data.Length);
                    compressionStream.Flush();
                }

                return target.ToArray();
            }
        }
    }
}
