using Microsoft.EntityFrameworkCore;

namespace vds_storage
{
    public class StorageDbContext : DbContext
    {
        public StorageDbContext()
        {

        }
        
        public StorageDbContext(DbContextOptions<StorageDbContext> options) : base(options)
        {
        }
    }
}
