namespace vds_storage
{
    public class StorageConfig
    {
        public StorageConfig()
        {
            this.RootPath = System.IO.Path.Combine(
                System.Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData),
                "VDS");

        }

        public string RootPath { get; set; }
    }
}