﻿using System;

namespace vds.channel
{
    public class message_channel
    {
        public static message_channel create_root_channel(string login, string password){
            var root_cert = cert_manager.create_root();
            Convert.ToBase64String(
                root_cert.Export(X509ContentType.Cert),
                Base64FormattingOptions.InsertLineBreaks));
            return new message_channel(root_cert);
        }
    }
}
