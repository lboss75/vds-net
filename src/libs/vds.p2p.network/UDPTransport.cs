﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace vds.p2p.network
{
    public class UDPTransport : IDisposable
    {
        private UdpClient client_;

        public void start(IPEndPoint endpoint){
            this.client_ = new UdpClient(endpoint);
        }

        public async Task<UdpReceiveResult> read_async() {
            return await this.client_.ReceiveAsync();
        }
        
        public void Dispose(){
            this.client_.Dispose();
        }
        
    }
}
