using System.Threading.Tasks;

namespace vds.p2p.network
{
    public interface IDatagramTransport
    {
         Task write_async(byte[] message);
    }
}