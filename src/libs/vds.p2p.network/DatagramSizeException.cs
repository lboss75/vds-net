namespace vds.p2p.network
{
    [System.Serializable]
    public class DatagramSizeException : System.Exception
    {
        public DatagramSizeException() { }
        public DatagramSizeException(string message) : base(message) { }
        public DatagramSizeException(string message, System.Exception inner)
         : base(message, inner) { }
        
    }
}