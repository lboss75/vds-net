using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using vds.p2p.network;

namespace vds.p2p.network
{
    public class DatagramProtocol
    {
        enum HeaderCommand
        {
            Data = 0,
            SingleData = 1,
            ContinueData = 2,
            Acknowledgment = 3,
        }

        ushort max_size_;
        IDatagramTransport target_;
        uint index_;
        SortedDictionary<uint, byte[]> outgoing_datagrams_ = new SortedDictionary<uint, byte[]>();

        uint next_index_;
        uint next_datagram_;
        Dictionary<uint, byte[]> datagrams_ = new Dictionary<uint, byte[]>();
        Func<byte[], Task> input_handler_;

        public DatagramProtocol(IDatagramTransport targer, Func<byte[], Task> input_handler)
        {
            this.target_ = targer;
            this.max_size_ = 65356;
            this.index_ = 0;

            this.input_handler_ = input_handler;
            this.next_index_ = 0;
            this.next_datagram_ = 0;

        }

        public async Task write_async(byte[] message)
        {
            for (;;)
            {
                try
                {
                    if (message.Length < this.max_size_ - 5)
                    {
                        var datagram = new byte[message.Length + 5];
                        datagram[0] = (byte) HeaderCommand.SingleData;
                        datagram[1] = (byte) (0xFF & this.index_ >> 24);
                        datagram[2] = (byte) (0xFF & this.index_ >> 16);
                        datagram[3] = (byte) (0xFF & this.index_ >> 8);
                        datagram[4] = (byte) (0xFF & this.index_);
                        message.CopyTo(datagram, 5);
                        await this.target_.write_async(datagram);
                        this.outgoing_datagrams_[this.index_++] = datagram;
                    }
                    else
                    {
                        int offset = 0;
                        while (offset < message.Length)
                        {
                            var size = Math.Min(this.max_size_, message.Length - offset + ((offset == 0) ? 8 : 5));
                            var datagram = new byte[size];
                            datagram[0] = (offset == 0)
                                ? (byte) HeaderCommand.Data
                                : (byte) HeaderCommand.ContinueData;
                            datagram[1] = (byte) (0xFF & this.index_ >> 24);
                            datagram[2] = (byte) (0xFF & this.index_ >> 16);
                            datagram[3] = (byte) (0xFF & this.index_ >> 8);
                            datagram[4] = (byte) (0xFF & this.index_);

                            if (0 == offset)
                            {
                                datagram[5] = (byte) (message.Length >> 16);
                                datagram[6] = (byte) (0xFF & message.Length >> 8);
                                datagram[7] = (byte) (0xFF & message.Length);
                                Array.Copy(message, 0, datagram, 8, size - 8);
                                offset += size - 8;
                            }
                            else
                            {
                                Array.Copy(message, offset, datagram, 5, size - 5);
                                offset += size - 5;
                            }

                            await this.target_.write_async(datagram);
                            this.outgoing_datagrams_[this.index_++] = datagram;
                        }

                    }

                    break;
                }
                catch (DatagramSizeException)
                {
                    this.max_size_ /= 2;
                }
            }
        }


        public async Task flush()
        {
            var datagram = new byte[5];
            datagram[0] = (byte) HeaderCommand.Acknowledgment;
            datagram[1] = (byte) (0xFF & this.index_ >> 24);
            datagram[2] = (byte) (0xFF & this.index_ >> 16);
            datagram[3] = (byte) (0xFF & this.index_ >> 8);
            datagram[4] = (byte) (0xFF & this.index_);

            await this.target_.write_async(datagram);
        }

        public async Task process(byte[] datagram)
        {
            switch ((HeaderCommand) datagram[0])
            {
                case HeaderCommand.Data:
                case HeaderCommand.SingleData:
                case HeaderCommand.ContinueData:
                {
                    var index = (uint) (datagram[1] << 24)
                                | (uint) (datagram[2] << 16)
                                | (uint) (datagram[3] << 8)
                                | (uint) datagram[4];
                    if (!this.datagrams_.ContainsKey(index))
                    {
                        this.datagrams_.Add(index, datagram);
                        if (index == this.next_index_)
                        {
                            while (this.datagrams_.ContainsKey(this.next_index_))
                            {
                                this.next_index_++;
                            }

                            while (this.datagrams_.TryGetValue(this.next_datagram_, out var item))
                            {
                                switch ((HeaderCommand) item[0])
                                {
                                    case HeaderCommand.SingleData:
                                    {
                                        var message = new byte[item.Length - 5];
                                        Array.Copy(item, 5, message, 0, message.Length);
                                        await this.input_handler_(message);
                                        this.next_datagram_++;
                                        break;
                                    }

                                    case HeaderCommand.Data:
                                    {
                                        var size = (uint) (uint) (item[5] << 16)
                                                   | (uint) (item[6] << 8)
                                                   | (uint) item[7];
                                        if (size <= item.Length - 8)
                                        {
                                            throw new InvalidDataException();

                                        }

                                        var left = size - (item.Length - 8);

                                        var current_index = this.next_datagram_ + 1;
                                        while (0 < left)
                                        {
                                            if (!this.datagrams_.TryGetValue(current_index, out var currrent_item))
                                            {
                                                return;
                                            }

                                            if (HeaderCommand.ContinueData != (HeaderCommand) currrent_item[0]
                                                || left < currrent_item.Length - 5)
                                            {
                                                throw new InvalidDataException();
                                            }

                                            left -= currrent_item.Length - 5;
                                            ++current_index;
                                        }

                                        var message = new byte[size];
                                        var offset = 0;
                                        for (var i = this.next_datagram_; i < current_index; ++i)
                                        {
                                            if (0 == offset)
                                            {
                                                Array.Copy(this.datagrams_[i], 8, message, offset,
                                                    this.datagrams_[i].Length - 8);
                                                offset += this.datagrams_[i].Length - 8;
                                            }
                                            else
                                            {
                                                Array.Copy(this.datagrams_[i], 5, message, offset,
                                                    this.datagrams_[i].Length - 5);
                                                offset += this.datagrams_[i].Length - 5;
                                            }
                                        }

                                        await this.input_handler_(message);
                                        this.next_datagram_ = current_index;
                                        break;
                                    }
                                }
                            }
                        }
                        else if (index > this.next_index_ + 32)
                        {
                            await this.sendAcknowledgment();
                        }
                    }

                    break;
                }
                case HeaderCommand.Acknowledgment:
                {
                    var index = (uint)(datagram[1] << 24)
                                | (uint)(datagram[2] << 16)
                                | (uint)(datagram[3] << 8)
                                | (uint)datagram[4];

                    var mask = (uint)(datagram[5] << 24)
                                | (uint)(datagram[6] << 16)
                                | (uint)(datagram[7] << 8)
                                | (uint)datagram[8];
                    for (;;)
                    {
                        if (this.outgoing_datagrams_.Count == 0)
                        {
                            break;
                        }

                        var findex = this.outgoing_datagrams_.Keys.First();
                        if (findex >= index)
                        {
                            break;
                        }

                        this.outgoing_datagrams_.Remove(findex);
                    }

                    for (uint i = 0; i < 32; ++i)
                    {
                        if (0 == (mask & 1))
                        {
                            if (this.outgoing_datagrams_.TryGetValue(index + i, out var item))
                            {
                                await this.target_.write_async(item);
                            }
                        }

                        mask >>= 1;
                    }
                    break;
                }
            }
        }

        public async Task timer()
        {
            await this.sendAcknowledgment();
        }

        private async Task sendAcknowledgment()
        {
            uint mask = 0;
            for (uint i = 0; i < 32; ++i)
            {
                mask >>= 1;
                if (this.datagrams_.ContainsKey(this.next_index_ + i))
                {
                    mask |= 0x80000000;
                }
            }

            var ack = new byte[9];
            ack[0] = (byte)HeaderCommand.Acknowledgment;
            ack[1] = (byte)(0xFF & this.next_index_ >> 24);
            ack[2] = (byte)(0xFF & this.next_index_ >> 16);
            ack[3] = (byte)(0xFF & this.next_index_ >> 8);
            ack[4] = (byte)(0xFF & this.next_index_);

            ack[5] = (byte)(0xFF & mask >> 24);
            ack[6] = (byte)(0xFF & mask >> 16);
            ack[7] = (byte)(0xFF & mask >> 8);
            ack[8] = (byte)(0xFF & mask);

            await this.target_.write_async(ack);
        }
    }
}