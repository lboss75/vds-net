using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using vds.p2p.network;
using Xunit;

namespace test.vds.p2p.network
{
    public class TestDatagramProtocol
    {
        [Fact]
        public void TestNetwork()
        {
            var rnd = new Random(DateTime.Now.Millisecond);
            byte[][] messages1 = new byte[1000][];
            for (int i = 0; i < 1000; ++i)
            {
                var size = rnd.Next(10, 1000);
                messages1[i] = new byte[size];
                for (int j = 0; j < size; ++j)
                {
                    messages1[i][j] = (byte) rnd.Next(0, 255);
                }
            }

            byte[][] messages2 = new byte[1000][];
            for (int i = 0; i < 1000; ++i)
            {
                var size = rnd.Next(10, 1000);
                messages2[i] = new byte[size];
                for (int j = 0; j < size; ++j)
                {
                    messages2[i][j] = (byte) rnd.Next(0, 255);
                }
            }

            var transport1 = new InvalidDatagramTransport(rnd);
            var transport2 = new InvalidDatagramTransport(rnd);
            int index1 = 0;
            var protocol1 = new DatagramProtocol(transport1, (datagram) =>
            {
                return Task.Factory.StartNew(() =>
                {
                    Assert.Equal(datagram.Length, messages2[index1].Length);

                    for (int j = 0; j < datagram.Length; ++j)
                    {
                        Assert.Equal(datagram[j], messages2[index1][j]);
                    }
                    Debug.WriteLine(index1);
                    ++index1;
                });
            });
            int index2 = 0;
            var protocol2 = new DatagramProtocol(transport2, (datagram) =>
            {
                return Task.Factory.StartNew(() =>
                {
                    Assert.Equal(datagram.Length, messages1[index2].Length);

                    for (int j = 0; j < datagram.Length; ++j)
                    {
                        Assert.Equal(datagram[j], messages1[index2][j]);
                    }
                    Debug.WriteLine(index2);
                    ++index2;
                });
            });

            var task1 = write_messages(protocol1, messages1);
            var task2 = write_messages(protocol2, messages2);

            var task3 = read_message(transport1, protocol2);
            var task4 = read_message(transport2, protocol1);

           
            Task.WaitAll(
                task1,
                transport1.write_async(null),
                transport2.write_async(null),
                task2,
                task3,
                task4);
        }

        async Task read_message(InvalidDatagramTransport transport, DatagramProtocol target)
        {
            for (;;)
            {
                var msg = await transport.read_async(() =>
                {
                    return target.timer();
                });

                if (null == msg)
                {
                    break;
                }

                await target.process(msg);
            }
        }

        async Task write_messages(DatagramProtocol transport, byte[][] messages){
            for(int i = 0; i < 1000; ++i){
                await transport.write_async(messages[i]);
            }
        }

        class InvalidDatagramTransport : IDatagramTransport
        {
            Random rnd_;
            ushort max_size_;
            List<byte[]> messages_ = new List<byte[]>();
            private bool is_eof_;

            public InvalidDatagramTransport(Random rnd){
                this.rnd_ = rnd;
                this.max_size_ = (ushort) rnd.Next(300, 1000);
            }

            public async Task write_async(byte[] message){
                if (message == null)
                {
                    this.is_eof_ = true;
                    return;
                }
                if(message.Length > this.max_size_){
                    throw new DatagramSizeException();
                }
                switch (this.rnd_.Next(0, 10))
                {
                    case 0://Ignore
                    break;

                    case 1://Reorder
                    lock(this.messages_){
                        messages_.Insert(this.rnd_.Next(0, this.messages_.Count), message);
                    }
                    break;

                    default:
                    lock(this.messages_){
                        messages_.Add(message);
                    }
                    break;
                }
            }

            public async Task<byte[]> read_async(Func<Task> timeout) {
                for(;;){
                    switch (this.rnd_.Next(0, 10)) {
                        case 0://Sleep
                        Thread.Sleep(1000);
                        break;
                    
                        default:
                        lock(this.messages_){
                            if(this.messages_.Count == 0){
                                if (this.is_eof_)
                                {
                                    return null;
                                }
                                Thread.Sleep(1000);
                                timeout().Wait();
                                continue;                                
                            }

                            var index = this.rnd_.Next(0, this.messages_.Count);
                            var result = this.messages_[index];
                            this.messages_.RemoveAt(index);

                            return result;                            
                        }
                    }
                }
            }
        }
    }
}
