using System;
using vds.crypto;
using Xunit;

namespace test.vds.crypto
{
    public class UnitTestCertManager
    {
        [Fact]
        public void TestCertCreate()
        {
            var cert = cert_manager.create_root();

            Assert.True(cert != null);
        }
    }
}
